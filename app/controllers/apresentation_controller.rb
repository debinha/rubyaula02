class ApresentationController < ApplicationController
    def home
        @num1 = params[:number1].to_i
        @num2 = params[:number2].to_i
    end

    def resultsoma 
        @result   = params[:number1].to_i + params[:number2].to_i
    end 
    def resultsub
        @result   = params[:number1].to_i - params[:number2].to_i
    end 
    def resultmult 
        @result   = params[:number1].to_i * params[:number2].to_i
    end 
    def resultdiv
        @result   = params[:number1].to_i / params[:number2].to_i
    end 
end