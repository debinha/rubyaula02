Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/apresentacao/:number1/:number2', to: 'apresentation#home'

  get '/apresentacao/:number1/:number2/soma', to: 'apresentation#resultsoma', as: 'som'

  get '/apresentacao/:number1/:number2/sub', to: 'apresentation#resultsub', as: 'sub'

  get '/apresentacao/:number1/:number2/mul', to: 'apresentation#resultmult', as: 'mul'

  get '/apresentacao/:number1/:number2/div', to: 'apresentation#resultdiv',as: 'div'
end
